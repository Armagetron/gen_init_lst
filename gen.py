from os import walk
import sys


def main():
    mypath = sys.argv[1]
    for (dirpath, dirnames, filenames) in walk(mypath):
        for file in filenames:
            fname = dirpath[len(mypath):] + '/' + file
            print('file ' + fname + ' ' + mypath + fname + ' 0755 0 0')


if __name__ == "__main__":
    main()
